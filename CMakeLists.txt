cmake_minimum_required(VERSION 3.5)

project(reaktor LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(DPP)
add_subdirectory(libjustlm)
add_subdirectory(colohalopp)
add_subdirectory(fmt)
add_subdirectory(commoncpp)

option(REAKTOR_WITH_CHAT "Enable model chat" No)

add_executable(reaktor
    main.cpp
    unicode_emojis.h)
target_link_libraries(reaktor PRIVATE commoncpp dpp justlm colohalopp fmt)
if (REAKTOR_WITH_CHAT)
    target_compile_definitions(reaktor PRIVATE REAKTOR_WITH_CHAT)
endif()
