# Reaktor
A Discord bot that automatically adds appropriate reactions to messages. It even understands jokes and puns, and is also able to make fun of things on its own.
It automatically makes sure to pop up here and there, but not excessively.

## Building
To compile the bot, clone this repository and be sure to initialize all submodules:

    git submodule update --init --depth 1 --recursive

Then, createa build directory, change into it, run CMake, and finally run make:

    mkdir build
    cd build
    cmake ..
    make -j$(nproc)

Now, there should be an executable called `reaktor`!

## Configuration
Most of the configuration happens via command line arguments. Just run the executable without any arguments to see a help page. For security reasons however, the bot token must be passed via either stdin or the environment variable `LMFUN_BOT_TOKEN`.

## Recommended model
This model has been giving me the best experience so far: https://huggingface.co/TheBloke/Luban-13B-GGUF/resolve/main/luban-13b.Q4_K_M.gguf

## Chat mode
If you want to use chat mode, set **-D**`REAKTOR_WITH_CHAT=Yes`. Then, when starting the bot, pass the ID of the channel to run the chat inside of, and a system prompt (see `system_prompt.txt` for example, be sure to keep the same format, multi-line messages are allowed).
